from loguru import logger
import unittest

from selenium import webdriver
from ddt import ddt, data, unpack
from hoverpy import capture
from pathlib import Path

import proxy_settings as proxy
from pages.main_page import MainPage
from pages.about_us import AboutUs, ContactsPanel
from pages.jobs import Jobs
from basic.common_methods import Common

# logger.configure(format='%(asctime)s - %(message)s', level=logging.INFO)
# logger = logging.getLogger('log')
# logger.setLevel(level=logging.INFO)


@ddt
class GameInsight(unittest.TestCase):
    """ТЕстирование GameInsight"""

    site = 'https://www.game-insight.com/'
    name = 'Dmitry'
    mail = 'test.avtotest@mail.ru'
    message = 'Скорее бы на работу'
    theme = 'Трудоустройство'
    # proxy.set_key('ProxyEnable', 1)
    # proxy.set_key('ProxyServer', u'http://localhost:8500')

    path = Path(__file__).resolve().parent
    contact_description = Common.get_text_from_doc(path, 'Contacts_description.ini')
    vacancy_description = Common.get_text_from_doc(path, 'Job_description.ini')

    def setUp(self):
        options = webdriver.ChromeOptions()
        options.add_argument("--start-maximized")
        self.driver = webdriver.Chrome(options=options)
        self.driver.get(self.site)
        self.driver.implicitly_wait(6)

        main_page = MainPage(self.driver)
        main_page.close_cookie_panel()
        Common.catch_errors(self.site)

    @capture(recordMode="once")
    def test_01_check_main_page(self):
        """Проверяем элементы главной страницы"""

        logger.info('Проверяем элементы главной страницы')
        main_page = MainPage(self.driver)
        main_page.check_main_elements()

    @data(
        ('The Tribez', 'Sim/Tycoon'),
        ('Airport Сity', 'Sim/Tycoon'),
        ('Mystery Manor', 'Mind Fitness'),
        ('Gods of Boom', 'Hardcore'),
        ('Paradise Island 2', 'Sim/Tycoon'),
        ('Trade Island', 'Sim/Tycoon'),
        ('Mirrors of Albion', 'Mind Fitness'),
        ('Treasure of Time', 'Mind Fitness'),

    )
    @unpack
    @capture(recordMode="once")
    def test_02_check_game_plates(self, game_name, game_genre):
        """Проверяем плашки игр"""

        logger.info('Авторизуемся на сайте')
        main_page = MainPage(self.driver)
        main_page.check_game_panels(game_name, game_genre)

    @data(
        ('For Media', contact_description[0]),
        ('For Developers', contact_description[1]),
        ('Other Questions', contact_description[2]),
        ('Tech Support', contact_description[3]),

    )
    @unpack
    @capture(recordMode="once")
    def test_03_check_contacts_block(self, block_name, description):
        """Проверяем блоки контактов"""

        logger.info('Проверяем блоки контактов')
        main_page = MainPage(self.driver)
        about_us = AboutUs(self.driver)
        main_page.select_tab('About Us')
        about_us.check_main_elements()
        about_us.check_contacts_block(block_name, description)
        about_us.open_contacts_form(block_name)
        contact_form = ContactsPanel(self.driver)
        contact_form.fill_contacts_form(self.name, self.mail, self.theme, self.message)
        contact_form.close_form()

    @capture(recordMode="once")
    def test_04_job_page(self):
        """Проверяем элементы страницы вакансий"""

        logger.info('Проверяем страницу вакансий')
        main_page = MainPage(self.driver)
        main_page.select_tab('Jobs')
        jobs = Jobs(self.driver)
        jobs.check_main_elements()
        jobs.check_filter('Departments', 'Development')
        logger.info('Заполняем форму о себе')
        jobs.fill_contacts_form(self.name, self.mail, '880005553535', self.message)

    @data(
        ('C++ Developer', vacancy_description[1], 'Development'),
        ('CI/CD Engineer', vacancy_description[0], 'Development')
    )
    @unpack
    @capture(recordMode="once")
    def test_05_check_job_block(self, block_name, description, type_of_vacancy):
        """Проверяем блоки вакансий"""

        logger.info('Проверяем блоки вакансий')
        main_page = MainPage(self.driver)
        main_page.select_tab('Jobs')
        jobs = Jobs(self.driver)
        jobs.check_vacancy_block(block_name, description, f'{type_of_vacancy}\nWFH 🏠')

    def tearDown(self):
        self.driver.close()
        Common.get_response_code(self.path, 'hoverfly.log', '40')
        Common.get_response_code(self.path, 'hoverfly.log', '50')


