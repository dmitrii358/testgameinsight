import logging

from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException


class Elements:
    """Поиск элементов"""
    def __init__(self, driver, rus_name: str = ''):
        self.driver = driver
        self.rus_name = rus_name

    def find_element(self, element, in_dom=True):
        """Обертка для поиска элементов

        :param element: стратегия поиска
        :param in_dom: есть элемент в дереве или нет
        """

        if in_dom:
            try:
                elements = self.driver.find_element(element['how'], element['locator'])
                return elements
            except NoSuchElementException:
                raise NoSuchElementException(logging.warning(f"Не найден элемент"
                                                             f" '{element['locator'], element['rus_name']}'"))
        else:
            try:
                elements = self.driver.find_element(element['how'], element['locator'])
                return elements
            except NoSuchElementException:
                pass

    def find_elements(self, element, in_dom=True):
        """Обертка для поиска элементов

        :param element: стратегия поиска
        :param in_dom: есть элемент в дереве или нет
        """

        if in_dom:
            try:
                elements = self.driver.find_elements(element['how'], element['locator'])
                return elements
            except NoSuchElementException:
                raise NoSuchElementException(logging.warning(f"Не найден элемент"
                                                             f" '{element['locator'], element['rus_name']}'"))
        else:
            try:
                elements = self.driver.find_elements(element['how'], element['locator'])
                return elements
            except NoSuchElementException:
                pass

    def get_element_from_element(self, parent, element_to_find):
        element = parent.find_element_by_css_selector(element_to_find)
        return element

    @staticmethod
    def element(how=By.CSS_SELECTOR, locator='', rus_name=''):
        """Обертка для поиска элементов

        :param how: стратегия поиска
        :param locator: локатор
        :param rus_name: название
        """

        element = {'how': how, 'locator': locator, 'rus_name': rus_name}
        return element

