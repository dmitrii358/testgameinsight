import os
import requests
import logging
import re

from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains

from basic.elements import Elements


class Common(Elements):
    """Базовые методы"""

    def check_presence(self, element, visibility=True, webelement=False):
        """Проверяем отображение элемента на странице

        :param element: элемент, которого ждем
        :param visibility:присутствует или нет
        :param webelement: передан webelement или нет
        """

        logging.debug(f'Проверям отображение элемента')
        wait = WebDriverWait(self.driver, 10)
        if webelement:
            elem = element
        else:
            elem = self.find_element(element)
        if visibility:
            wait.until(EC.visibility_of(elem), 'Не отображается элемент')
        else:
            wait.until(EC.invisibility_of_element(elem), 'Элемент не должен отображаться')

    def check_text_in_element(self, element, text, webelement=False):
        """Проверяем наличие текста в элементе

        :param element: элемент, которого ждем
        :param text:текст в элементе
        :param webelement: передан webelement или нет
        """

        wait = WebDriverWait(self.driver, 1)
        if webelement:
            assert element.text == text, f'Текст {text} не отображается в элементе'
        else:
            wait.until(EC.text_to_be_present_in_element((element['how'], element['locator']), text),
                       f'Не найден текст в элементе {self.rus_name}')

    def select_from_list_of_elements(self, elements, name):
        """Выбор элемента из группы одинаковых элементов

        :param elements: элемент, которого ждем
        :param name: текст в элементе
        """

        field_data = list(filter(lambda elem: name in elem.get_attribute('innerHTML'), elements))
        self.driver.implicitly_wait(2)
        return field_data[0]

    def mouse_over(self, element):
        """Навести на элемент

        :param element: элемент, которого ждем
        """
        hover = ActionChains(self.driver).move_to_element(element)
        hover.perform()

    @staticmethod
    def get_text_from_doc(path='', file_name=''):
        """Получаем строку из файла"""

        line_list = []
        with open(os.path.join(path, file_name), 'r') as f:
            while True:
                line = f.readline()
                if not line: break
                new_line = line[:-1]
                line_list.append(r"{}".format(new_line))

        return line_list

    @staticmethod
    def catch_errors(url):
        r = requests.get(url)
        r.raise_for_status()

    @staticmethod
    def get_response_code(path, file_name, pattern):
        """Получаем коды ответов"""

        errors = []
        with open(os.path.join(path, file_name), 'r') as f:
            for i, line in enumerate(f):
                for match in re.finditer('HTTP/1.1" ' + pattern, line):
                    errors.append('Найдены 400 или 500 ошибки в логах на строке %s: %s' % (i + 1, match.group()))
        f.close()
        logging.info(errors)










