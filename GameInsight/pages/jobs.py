from loguru import logger

from selenium.webdriver.common.by import By

from basic.elements import Elements
from basic.common_methods import Common


class Jobs(Elements):
    """Страница работа(Не с русским языком)"""

    vacancy_pages               = Elements.element(      By.CSS_SELECTOR,  '.page-vacancies', 'Страница вакансий')
    vacany_filter               = Elements.element(      By.CSS_SELECTOR,  '.filter', 'Фильтр')
    filter_item                 = Elements.element(      By.CSS_SELECTOR,  '.filter-list__item', 'Элементы фильтра')
    filter_type                 = Elements.element(      By.CSS_SELECTOR,  '.filter__item-title', 'Тип фильтра')
    filter_list                 = Elements.element(      By.CSS_SELECTOR,  '.filter-list', 'Выпадающий список фильтров')

    vacancy_column              = Elements.element(      By.CSS_SELECTOR,  '.columns__item', 'Вакансии ')
    vacancy_name                = Elements.element(      By.CSS_SELECTOR,  '.columns__item-h1', 'Название вакансии')
    vacancy_type                = Elements.element(      By.CSS_SELECTOR,  '.columns__item-subtitle', 'Направление в вакансии')
    vacancy_description         = Elements.element(      By.CSS_SELECTOR,  '.columns__item-desc', 'Описание вакансии')
    apply                       = Elements.element(      By.CSS_SELECTOR,  '.columns__item-respond-button', 'Откликнуться на вакансию')
    more                        = Elements.element(      By.CSS_SELECTOR,  '.columns__item-more',  'Подробнее')
    banner                      = Elements.element(      By.CSS_SELECTOR,  '.columns-img__banner', 'Логотип')

    # Раздел о себе
    about_yourself_block        =  Elements.element(     By.CSS_SELECTOR,  '.gi-form-container',   'блок о себе')
    name                        =  Elements.element(     By.CSS_SELECTOR,  '[name="application_components_GIFormJoin[name]"]',   'Имя')
    email                       =  Elements.element(     By.CSS_SELECTOR,  '[name="application_components_GIFormJoin[email]"]',  'Почта')
    contact_phone               =  Elements.element(     By.CSS_SELECTOR,  '[name="application_components_GIFormJoin[phone]"]',  'Телефон')
    additional_info             =  Elements.element(     By.CSS_SELECTOR,  '[name="application_components_GIFormJoin[info]"]',   'Доп. инфо')
    accept_processing_personal_data =  Elements.element( By.CSS_SELECTOR,  '[name="application_components_GIFormJoin[accept]"]', 'Принять согласение на обработку данных')
    captcha_checkbox            =  Elements.element(     By.CSS_SELECTOR,  '.recaptcha-checkbox', 'Капча')
    submit                      =  Elements.element(     By.CSS_SELECTOR,  '.form-submit', 'Отправить')
    attach                      =  Elements.element(     By.CSS_SELECTOR,  '.form__item-attach-button', 'Прикрепить')

    def check_main_elements(self):
        """Проверяем блоки основных элементов
        """

        logger.info('Проверяем блоки основных элементов')
        common_methods = Common(self.driver)
        common_methods.check_presence(self.vacancy_pages)
        common_methods.check_presence(self.vacany_filter)
        common_methods.check_presence(self.banner)

    def check_vacancy_block(self, block_name, description_text, field):
        """Проверяем отображение проверяем блок контактов

        :param block_name: название блока
        :param description_text: описание
        :param field: поле деятельности
        """

        logger.info('Проверяем отображение блоков вакансии')
        elements = Elements(self.driver)
        common_methods = Common(self.driver)
        get_element = common_methods.get_element_from_element
        contact_block = common_methods.select_from_list_of_elements(elements.find_elements(self.vacancy_column),
                                                                    block_name)
        common_methods.mouse_over(contact_block)

        logger.info('Проверяем отображение Содержимого вакансии')
        description = get_element(contact_block, self.vacancy_description['locator'])
        common_methods.check_text_in_element(description, description_text, webelement=True)
        vacancy_type = get_element(contact_block, self.vacancy_type['locator'])
        common_methods.check_text_in_element(vacancy_type, field, webelement=True)

        logger.info('Проверяем отображение кнопок в колонке вакансии')
        apply = get_element(contact_block, self.apply['locator'])
        common_methods.check_presence(apply, webelement=True)
        more = get_element(contact_block, self.more['locator'])
        common_methods.check_presence(more, webelement=True)

    def fill_contacts_form(self, name, email, phone='', info=''):
        """Проверяем заполнение форму контактов

        :param name: имя
        :param email: почта
        :param phone: телефон
        :param info: доп инфо
        """

        logger.info('Проверяем заполнение формы контактов')
        common_methods = Common(self.driver)
        common_methods.check_presence(self.about_yourself_block)
        element = Elements(self.driver).find_element
        common_methods.mouse_over(element(self.about_yourself_block))
        element(self.name).send_keys(name)
        element(self.email).send_keys(email)
        if phone:
            element(self.contact_phone).send_keys(phone)
        if info:
            element(self.additional_info).send_keys(info)
        element(self.accept_processing_personal_data).click()
        common_methods.check_presence(self.submit)
        element(self.attach).click()

    def check_filter(self, filter_type_name, filter_name):
        """Проверяем отображение проверяем блок контактов

        :param filter_type_name: тип фильтра
        :param filter_name: название фильтра
        """

        logger.info('Проверяем отработку фильтрации')
        elements = Elements(self.driver)
        common_methods = Common(self.driver)
        get_element = common_methods.get_element_from_element
        type_of_filter = common_methods.select_from_list_of_elements(elements.find_elements(self.filter_type),
                                                                     filter_type_name)
        type_of_filter.click()
        my_filter = common_methods.select_from_list_of_elements(elements.find_elements(self.filter_item), filter_name)
        my_filter.click()

        vacancy_block = elements.find_elements(self.vacancy_column)
        vacancy_type_list = list(map(lambda block: get_element(block, self.vacancy_type['locator']).text, vacancy_block))
        for vacancy in vacancy_type_list:
            if vacancy != "":
                assert filter_name in vacancy, 'Отображаются вакансии, не входящие в фильтрацию'






