from loguru import logger

from selenium.webdriver.common.by import By

from basic.elements import Elements
from basic.common_methods import Common


class AboutUs(Elements):
    """Страница о нас"""

    logo_panel               = Elements.element(      By.CSS_SELECTOR,  '.page-content-studio .js-image', 'Панель лого')
    studio_logo              = Elements.element(      By.CSS_SELECTOR,  '.studio-logo', 'Логотип')
    about_us_block           = Elements.element(      By.CSS_SELECTOR,  '.studio', 'О нас')
    contact_us_block         = Elements.element(      By.CSS_SELECTOR,  '.studio_contact_item', 'Блоки для связи ')
    contact_us_block_title   = Elements.element(      By.CSS_SELECTOR,  '.studio_contact_item-title', 'Заголовок блока')
    contact_us_block_descr   = Elements.element(      By.CSS_SELECTOR,  '.studio_contact_item-desc', 'Описание блока ')
    contacts                 = Elements.element(      By.CSS_SELECTOR,  '.studio_contact_item-button', 'Кнопка контакты ')

    def check_main_elements(self):
        """Проверяем блоки основных элементов
        """

        logger.info('Проверяем блоки основных элементов')
        common_methods = Common(self.driver)
        common_methods.check_presence(self.logo_panel)
        common_methods.check_presence(self.studio_logo)
        common_methods.check_presence(self.about_us_block)

    def check_contacts_block(self, block_name, description_text):
        """Проверяем отображение проверяем блок контактов

        :param block_name: название блока
        :param description_text: описание
        """

        logger.info('Проверяем отображение блоков контактов')
        elements = Elements(self.driver)
        common_methods = Common(self.driver)
        contact_block = common_methods.select_from_list_of_elements(elements.find_elements(self.contact_us_block),
                                                                    block_name)
        common_methods.mouse_over(contact_block)
        description = common_methods.get_element_from_element(contact_block, self.contact_us_block_descr['locator'])
        common_methods.check_text_in_element(description, description_text, webelement=True)

    def open_contacts_form(self, block_name):
        """Проверяем отображение проверяем блок контактов

        :param block_name: название блока
            """

        logger.info('Проверяем отображение блоков контактов')
        elements = Elements(self.driver)
        common_methods = Common(self.driver)
        contact_block = common_methods.select_from_list_of_elements(elements.find_elements(self.contact_us_block),
                                                                        block_name)
        common_methods.mouse_over(contact_block)
        contact_btn = common_methods.get_element_from_element(contact_block, self.contacts['locator'])
        contact_btn.click()


class ContactsPanel(Elements):
    """Форма отправки контактов"""

    contact_panel      =  Elements.element(     By.CSS_SELECTOR,  '.form-wrapper', 'Форма отправки контактов')
    close_panel        =  Elements.element(     By.CSS_SELECTOR,  '.form-cross', 'Закрыть форму')
    name               =  Elements.element(     By.CSS_SELECTOR,  '[name="ContactMailForm[name]"]', 'Имя')
    email              =  Elements.element(     By.CSS_SELECTOR,  '[name="ContactMailForm[email]"]', 'Почта')
    subject            =  Elements.element(     By.CSS_SELECTOR,  '[name="ContactMailForm[subject]"]', 'Тема')
    message            =  Elements.element(     By.CSS_SELECTOR,  '[name="ContactMailForm[message]"]', 'Сообщение')
    captcha_checkbox   =  Elements.element(     By.CSS_SELECTOR,  '.recaptcha-checkbox', 'Капча')
    submit             =  Elements.element(     By.CSS_SELECTOR,  '.form-submit', 'Отправить')

    def fill_contacts_form(self, name, email, subject, message):
        """Проверяем заполнение формы контактов"""

        logger.info('Проверяем заполнение формы контактов')
        common_methods = Common(self.driver)
        common_methods.check_presence(self.contact_panel)
        element = Elements(self.driver).find_element
        element(self.name).send_keys(name)
        element(self.email).send_keys(email)
        element(self.subject).send_keys(subject)
        element(self.message).send_keys(message)
        common_methods.check_presence(self.submit)

    def close_form(self):
        """Закрыть форму контактов"""

        logger.info('Закрыть форму контактов')
        common_methods = Common(self.driver)
        element = Elements(self.driver).find_element
        element(self.close_panel).click()
        common_methods.check_presence(self.contact_panel, False)
