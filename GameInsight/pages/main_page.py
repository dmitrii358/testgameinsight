from loguru import logger

from selenium.webdriver.common.by import By

from basic.elements import Elements
from basic.common_methods import Common


class MainPage(Elements):
    """Главная страница"""

    page_lang         = Elements.element(By.CSS_SELECTOR,  '[lang]', 'Язык страницы')
    lang_choosing     = Elements.element(By.CSS_SELECTOR,  '.header-lang-wrapper', 'Выбор языка')
    header_panel      = Elements.element(By.CSS_SELECTOR,  '[id="header"]', 'Шапка на главной странице')
    header_items      = Elements.element(By.CSS_SELECTOR,  '.header-nav-item', 'Пункты в меню шапки')
    expanded_header   = Elements.element(By.CSS_SELECTOR,  '.header-nav-item__inner-links', 'Развернутая шапка')

    main_panel        = Elements.element(By.CSS_SELECTOR,  '.page-main', 'Панель главной страницы')
    game_panels_block = Elements.element(By.CSS_SELECTOR,  '.plate_theme_games', 'Блок игр')
    game_panels_list  = Elements.element(By.CSS_SELECTOR,  '.plate__link', 'Список плашек с играми')
    game_panel_name   = Elements.element(By.CSS_SELECTOR,  '.plate__title', 'Название плашки')
    game_panel_tag    = Elements.element(By.CSS_SELECTOR,  '.plate__tag', 'Название тэга')
    game_panel_frame  = Elements.element(By.CSS_SELECTOR,  '.video_frame:not(.hidden)', 'Видео-фрейм')
    game_panel_more   = Elements.element(By.CSS_SELECTOR,  '.plate__popup', 'Всплывашка еще')

    next_page         = Elements.element(By.CSS_SELECTOR,  '.loading__wrapper', 'следующая страница')
    news              = Elements.element(By.CSS_SELECTOR,  '.news-slider', 'Новости')
    slogan            = Elements.element(By.CSS_SELECTOR,  '.slogan', 'Слоган')
    policy            = Elements.element(By.CSS_SELECTOR,  '.footer-top-link', 'Политика')
    support_block     = Elements.element(By.CSS_SELECTOR,  '.footer-middle-wrapper', 'Политика')
    cookie_notice     = Elements.element(By.CSS_SELECTOR,  '.cookie-wrapper', 'Информация о куках')
    cookie_close      = Elements.element(By.CSS_SELECTOR,  '.cookie-cross-svg', 'Закрыть куки')
    social_network    = Elements.element(By.CSS_SELECTOR,  '.footer-bottom-wrapper', 'Соц. сети')

    def check_main_elements(self):
        """Проверяем блоки основных элементов
        """

        logger.info('Проверяем блоки основных элементов')
        common_methods = Common(self.driver)
        common_methods.check_presence(self.page_lang)
        common_methods.check_presence(self.header_panel)
        common_methods.check_presence(self.game_panels_block)
        common_methods.check_presence(self.news)
        common_methods.check_presence(self.slogan)
        common_methods.check_presence(self.policy)
        common_methods.check_presence(self.news)
        common_methods.check_presence(self.next_page)
        common_methods.check_presence(self.support_block)
        common_methods.check_presence(self.social_network)

    def check_expanding(self, tab_name):
        """Проверяем раскрытие блока меню вкладок

        :param tab_name: имя закладки
        """

        logger.info('Проверяем раскрытие блока меню вкладок')
        elements = Elements(self.driver).find_elements
        common_methods = Common(self.driver)
        tab = common_methods.select_from_list_of_elements(elements(self.header_items), tab_name)
        common_methods.mouse_over(tab)
        common_methods.check_presence(self.expanded_header)

    def check_game_panels(self, name, genre):
        """Проверяем отображение элементов панелей игр

        :param name: название игры
        :param genre: жанр
        """

        logger.info('Проверяем отображение элементов панелей игр')
        elements = Elements(self.driver)
        common_methods = Common(self.driver)
        game = common_methods.select_from_list_of_elements(elements.find_elements(self.game_panels_list), name)
        common_methods.mouse_over(game)

        logger.info('Проверяем название и жанр')
        game_name = common_methods.get_element_from_element(game, self.game_panel_name['locator'])
        common_methods.check_text_in_element(game_name, name.upper(), True)
        game_genre = common_methods.get_element_from_element(game, self.game_panel_tag['locator'])
        common_methods.check_text_in_element(game_genre, genre, True)
        common_methods.mouse_over(game)
        common_methods.check_presence(self.game_panel_frame)

        logger.info('Проверяем всплывашку "показать еще"')
        common_methods.check_presence(self.game_panel_frame)
        more_panel = self.get_element_from_element(game, self.game_panel_more['locator'])
        common_methods.check_text_in_element(more_panel, 'MORE', True)

    def select_tab(self, tab_name):
        """Переходим по вкладкам
        """

        logger.info('Проверяем блоки основных элементов')
        common_methods = Common(self.driver)
        elements = Elements(self.driver).find_elements
        tab = common_methods.select_from_list_of_elements(elements(self.header_items), tab_name)
        tab.click()
        common_methods.catch_errors(self.driver.current_url)

    def close_cookie_panel(self):
        """Закрыть панель уведомления о куки
        """

        logger.info('Проверяем и закрываем панель куки')
        common_methods = Common(self.driver)
        common_methods.check_presence(self.cookie_notice)
        elements = Elements(self.driver).find_element
        elements(self.cookie_close).click()
